from os import name
import re
from flask import Flask, render_template, request, url_for, redirect
import csv

app = Flask(__name__)
# Not working --> app.config['DEBUG'] = True
# print(__name__) --> this gives __main__


@app.route("/")
def my_home():
    return render_template('index.html')


# @app.route("/index.html")
# def index():
#     return render_template('index.html')


# @app.route("/about.html")
# def about():
#     return render_template('about.html')


# @app.route("/work.html")
# def work():
#     return render_template('work.html')


# @app.route("/works.html")
# def works():
#     return render_template('works.html')


# @app.route("/contact.html")
# def contact():
#     return render_template('contact.html')


# @app.route("/components.html")
# def componentsk():
#     return render_template('components.html')


# instead of the above, we can use the variable
@app.route('/<string:page_name>')
def html_page(page_name):
    return render_template(page_name)


def write_to_file(data):
    with open('database.txt', mode='a') as database:
        email = data['email']
        subject = data['subject']
        message = data['message']
        file = database.write(f'\n{email},{subject},{message}')


def write_to_CSVfile(data):
    with open('database.csv', newline='', mode='a') as database2:
        email = data['email']
        subject = data['subject']
        message = data['message']
        csv_writer = csv.writer(database2, delimiter=',',
                                quotechar=';', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow([email, subject, message]
                            )
    database2.close


# the below submit_form url will be added by form_action in contact.html using post method,
# which means the URL will post a data to the server
@app.route('/submit_form', methods=['POST', 'GET'])
def submit():
    if request.method == 'POST':
        try:
            data = request.form.to_dict()
            write_to_CSVfile(data)
            return redirect('/thankyou.html')
        except:
            return 'something went wrong in writing'
    else:
        return 'something went wrong. Try again!'
